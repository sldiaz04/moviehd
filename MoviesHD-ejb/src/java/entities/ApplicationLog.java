/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sldia
 */
@Entity
@Table(name = "application_log")
@XmlRootElement
public class ApplicationLog implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "LEVEL")
    private String level;
    @Size(max = 100)
    @Column(name = "LOGGERNAME")
    private String logerName;
    @Size(max = 100)
    @Column(name = "MESSAGE")
    private String message;
    @Size(max = 100)
    @Column(name = "SOURCE")
    private String source;
    @Size(max = 100)
    @Column(name = "TIMEMILLIS")
    private String timeMillis;
    
    public Integer getId() {
        return id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getLevel() {
        return level;
    }

    public String getLogerName() {
        return logerName;
    }

    public String getMessage() {
        return message;
    }
    
    public String getSource() {
        return source;
    }

    public String getTimeMillis() {
        return timeMillis;
    }
    
}
