/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package purchase;

import cart.CartSessionBean;
import entities.Client;
import entities.Detail;
import entities.Invoce;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.LocalBean;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Remove;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sessionBeans.CartFacade;
import sessionBeans.DetailFacade;
import sessionBeans.InvoceFacade;
import singletons.SingletonStatistics;

/**
 *
 * @author sldia
 */
@Stateful
@LocalBean
public class PurchaseSessionBean {

    @EJB
    private CartFacade cartFacade;

    @EJB
    private SingletonStatistics singletonStatistics;

    @EJB
    private CartSessionBean cartSessionBean;

    @EJB
    private InvoceFacade invoceFacade;
    
    @EJB
    private DetailFacade detailFacade;
    
    private static Logger LOGGER;
    
    @PostConstruct
    void postConstruct(){
        LOGGER = LogManager.getLogger("singleton.stateful");
        LOGGER.info(this);
    }
    
    @PrePassivate
    void prePassivate() {
        LOGGER.info(this);
    }

    @PostActivate
    void postActivate() {
        LOGGER.info(this);
    }

    @Remove
    public void remove() {
        LOGGER.info(this);
    }
    
    @PreDestroy
    void preDestroy() {
        LOGGER.info(this);
    }
    
    public void makePayment(CartSessionBean csb, Client client){
        Invoce invoce = new Invoce();
        invoce.setPurchaseDate(new Date());
        invoce.setIdClient(client);
        invoce.setPrice(0);
        invoceFacade.create(invoce);

        csb.getItems().stream().map((cartLine1) -> {
            Detail detail = new Detail();
            detail.setIdMovie(cartLine1.getMovie());
            detail.setAmount(cartLine1.getAmount());
            detail.setIdInvoce(invoce);
            detail.setPrice(cartLine1.getTotalPrice());
            detail.setDiscount(cartLine1.getDiscount());
            return detail;
        }).forEachOrdered((detail) -> {
            detailFacade.create(detail);
        });
        invoce.setPrice(detailFacade.getTotalInvoce(invoce));
        invoceFacade.edit(invoce);
        cartFacade.deleteCartsFromClient(client);
        cartSessionBean.remove();
        
        // Logs
        LOGGER.info(client.getName() + " " + client.getLastname());
        singletonStatistics.addComponetsPerUser(client.getName() + " " + client.getLastname());
        singletonStatistics.addVisitsPerPagesAndComponents(PurchaseSessionBean.class.getName());
    }
    
    public List<Invoce> getInvoces(Client client){
        // Logs
        LOGGER.info(client.getName() + " " + client.getLastname());
        singletonStatistics.addComponetsPerUser(client.getName() + " " + client.getLastname());
        singletonStatistics.addVisitsPerPagesAndComponents(PurchaseSessionBean.class.getName());
        return invoceFacade.getInvocesFromUser(client);
    }
}
