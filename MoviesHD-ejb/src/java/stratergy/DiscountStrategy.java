/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stratergy;

import cart.CartLine;

/**
 *
 * @author sldia
 */
public abstract class DiscountStrategy {
    public abstract void applyDiscount(CartLine cartLine);
}
