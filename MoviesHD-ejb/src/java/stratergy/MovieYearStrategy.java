/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stratergy;

import cart.CartLine;

/**
 *
 * @author sldia
 */
public class MovieYearStrategy extends DiscountStrategy{

    @Override
    public void applyDiscount(CartLine cartLine) {
        double moviePrice = cartLine.getMovie().getPrice();
        if (cartLine.getMovie().getYear() <= 2013) {
            double discount = moviePrice * 0.10;
            cartLine.getMovie().setPrice(moviePrice - discount);
            cartLine.setDiscount(discount);
        }
    }
    
}
