/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singletons;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.ejb.Schedule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sldia
 */
@Singleton
@LocalBean
public class SingletonStatistics {

    private Map<String, Integer> componentPerUsers;
    private HashMap<String, Integer> visitsPerPagesAndComponets;
    private HashMap<String, Integer> visitsPerPagesAndComponetsCopy;
    private Integer numberUsersLoggin;

    private static Logger loggerSingletonStatistic;
    private static Logger loggerSingletonLogs;
    private static Logger loggerDataBase;

    @PostConstruct
    void postConstruct() {
        loggerSingletonStatistic = LogManager.getLogger("singletonStatistics");
        loggerSingletonLogs = LogManager.getLogger("singleton");
        loggerDataBase = LogManager.getLogger("database");
        componentPerUsers = new HashMap<>();
        visitsPerPagesAndComponets = new HashMap<>();
        visitsPerPagesAndComponetsCopy = (HashMap<String, Integer>) visitsPerPagesAndComponets.clone();
        numberUsersLoggin = 0;
    }

    @PreDestroy
    void preDestroy() {
    }

    public void incremetUserLoggin() {
        numberUsersLoggin++;
    }

    public void addComponetsPerUser(String userId) {
        if (componentPerUsers.containsKey(userId)) {
            componentPerUsers.put(userId, componentPerUsers.get(userId) + 1);
        } else {
            componentPerUsers.put(userId, 1);
        }
    }

    public void addVisitsPerPagesAndComponents(String pageOrComppnentsName) {
        if (visitsPerPagesAndComponets.containsKey(pageOrComppnentsName)) {
            visitsPerPagesAndComponets.put(pageOrComppnentsName, visitsPerPagesAndComponets.get(pageOrComppnentsName) + 1);
        } else {
            visitsPerPagesAndComponets.put(pageOrComppnentsName, 1);
        }
    }

    @Schedule(second = "*/10", minute = "*", hour = "*")
    public void scheduleTimer() {
        Iterator<Entry<String, Integer>> it = componentPerUsers.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, Integer> e = it.next();
            loggerSingletonStatistic.info("User: {} --- Component count: {}", e.getKey(), e.getValue());
        }

        Iterator<Entry<String, Integer>> iterator = visitsPerPagesAndComponets.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<String, Integer> e = iterator.next();
            loggerSingletonStatistic.info("Pages or components: {} --- Visits count: {}", e.getKey(), e.getValue());
            loggerDataBase.info("Pages or components: {} --- Visits count: {}", e.getKey(), e.getValue());
        }
        loggerSingletonStatistic.info("User loggin: {}", numberUsersLoggin);
    }

    @Schedule(second = "*/5", minute = "*", hour = "*")
    public void scheduleTimer2() {
        if (visitsPerPagesAndComponetsCopy.equals(visitsPerPagesAndComponets)) {
            loggerSingletonLogs.info("Estoy ocioso");
        } else {
            visitsPerPagesAndComponetsCopy = (HashMap<String, Integer>) visitsPerPagesAndComponets.clone();
        }
    }
}
