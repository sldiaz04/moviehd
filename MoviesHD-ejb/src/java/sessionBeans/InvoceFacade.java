/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionBeans;

import entities.Client;
import entities.Invoce;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import singletons.SingletonStatistics;

/**
 *
 * @author sldia
 */
@Stateless
public class InvoceFacade extends AbstractFacade<Invoce> {

    @EJB
    private SingletonStatistics singletonStatistics;

    @PersistenceContext(unitName = "MoviesHD-ejbPU")
    private EntityManager em;

    private static Logger LOGGER;
    
    @PostConstruct
    void postConstruct() {
        LOGGER = LogManager.getLogger("singleton.stateless");
        LOGGER.info(this);
    }

    @PreDestroy
    void preDestroy() {
        LOGGER.info(this);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InvoceFacade() {
        super(Invoce.class);
    }
    
    public List<Invoce> getInvocesFromUser(Client client){
        // Logs
        LOGGER.info(client.getName() + " " + client.getLastname());
        singletonStatistics.addComponetsPerUser(client.getName() + " " + client.getLastname());
        singletonStatistics.addVisitsPerPagesAndComponents(InvoceFacade.class.getName());
        
        Query q = em.createQuery("SELECT i FROM Invoce i WHERE i.idClient = :client");
        q.setParameter("client", client);
        return q.getResultList();
    }
    
}
