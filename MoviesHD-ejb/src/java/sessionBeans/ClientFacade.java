/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionBeans;

import entities.Client;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import singletons.SingletonStatistics;

/**
 *
 * @author sldia
 */
@Stateless
public class ClientFacade extends AbstractFacade<Client> {

    @EJB
    private SingletonStatistics singletonStatistics;
    private static Logger LOGGER;

    @PersistenceContext(unitName = "MoviesHD-ejbPU")
    private EntityManager em;
    

    @PostConstruct
    void postConstruct() {
        LOGGER = LogManager.getLogger("singleton.stateless");
        LOGGER.info(this);
    }

    @PreDestroy
    void preDestroy() {
        LOGGER.info(this);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public ClientFacade() {
        super(Client.class);
    }
    
    public Client getClientByEmail(String email) {
        singletonStatistics.addVisitsPerPagesAndComponents(ClientFacade.class.getName());
        Query q = em.createNamedQuery("Client.findByEmail");
        q.setParameter("email", email);
        Client client = null;
        try {
            client = (Client) q.getSingleResult();
        } catch (NoResultException e) {
        }
        return client;
    }
}
