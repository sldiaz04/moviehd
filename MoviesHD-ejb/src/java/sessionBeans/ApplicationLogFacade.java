/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionBeans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import entities.ApplicationLog;

/**
 *
 * @author sldia
 */
@Stateless
public class ApplicationLogFacade extends AbstractFacade<ApplicationLog>{

    private static Logger LOGGER;
    
    @PersistenceContext(unitName = "MoviesHD-ejbPU")
    private EntityManager em;

    @PostConstruct
    void postConstruct() {
        LOGGER = LogManager.getLogger("singleton.stateless");
        LOGGER.info(this);
    }

    @PreDestroy
    void preDestroy() {
        LOGGER.info(this);
    }
    
    public ApplicationLogFacade() {
        super(ApplicationLog.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
