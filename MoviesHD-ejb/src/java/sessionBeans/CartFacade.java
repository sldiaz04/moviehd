/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionBeans;

import entities.Cart;
import entities.Client;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author sldia
 */
@Stateless
public class CartFacade extends AbstractFacade<Cart> {

    @PersistenceContext(unitName = "MoviesHD-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CartFacade() {
        super(Cart.class);
    }
    
    public List<Cart> getCartsFromClient(Client client) {
        // JPQL
        /*Query q = em.createQuery("SELECT c FROM Cart c WHERE c.idClient = :client");
        q.setParameter("client", client);
        return q.getResultList();*/
        
        // Criteria API
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Cart> cq = cb.createQuery(Cart.class);
        Root<Cart> root = cq.from(Cart.class);
        cq.select(root).where(cb.equal(root.get("idClient"), client));
        TypedQuery<Cart> tq = em.createQuery(cq);
        return tq.getResultList();
    }
    
    public void deleteCartsFromClient(Client client) {
        // JPQL
        /*Query q = em.createQuery("DELETE  FROM Cart c WHERE c.idClient = :client");
        q.setParameter("client", client);
        q.executeUpdate();*/
        
        // Criteria API
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaDelete<Cart> delete = cb.createCriteriaDelete(Cart.class);
        Root<Cart> root = delete.from(Cart.class);
        delete.where(cb.equal(root.get("idClient"), client));
        em.createQuery(delete).executeUpdate();
    }
}
