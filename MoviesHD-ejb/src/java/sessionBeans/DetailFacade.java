/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionBeans;

import entities.Detail;
import entities.Invoce;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import singletons.SingletonStatistics;

/**
 *
 * @author sldia
 */
@Stateless
public class DetailFacade extends AbstractFacade<Detail> {

    @EJB
    private SingletonStatistics singletonStatistics;

    @PersistenceContext(unitName = "MoviesHD-ejbPU")
    private EntityManager em;
    
    private static Logger LOGGER;
    
    @PostConstruct
    void postConstruct() {
        LOGGER = LogManager.getLogger("singleton.stateless");
        LOGGER.info(this);
    }

    @PreDestroy
    void preDestroy() {
        LOGGER.info(this);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetailFacade() {
        super(Detail.class);
    }
    
    public Double getTotalInvoce(Invoce invoce) {
        singletonStatistics.addVisitsPerPagesAndComponents(DetailFacade.class.getName());
        Query query = em.createQuery("SELECT SUM(d.price) FROM Detail d WHERE d.idInvoce = :idinvoce");
        query.setParameter("idinvoce", invoce);
        return (Double) query.getSingleResult();
    }
    
    public List<Detail> getDetailsFromInvoce(Invoce invoce){
        singletonStatistics.addVisitsPerPagesAndComponents(DetailFacade.class.getName());
        Query q = em.createQuery("SELECT d FROM Detail d WHERE d.idInvoce = :invoce");
        q.setParameter("invoce", invoce);
        return q.getResultList();
    }
}
