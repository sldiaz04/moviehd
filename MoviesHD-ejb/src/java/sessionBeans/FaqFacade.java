/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionBeans;

import entities.Faq;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author sldia
 */
@Stateless
public class FaqFacade extends AbstractFacade<Faq> {

    @PersistenceContext(unitName = "MoviesHD-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FaqFacade() {
        super(Faq.class);
    }
    
    public List<Faq> getAllFAQs(){
        CriteriaBuilder cb = em.getCriteriaBuilder();   
        CriteriaQuery<Faq> cq = cb.createQuery(Faq.class);
        Root<Faq> from = cq.from(Faq.class);
        cq.select(from);
        TypedQuery<Faq> tq = em.createQuery(cq);
        return tq.getResultList();
    }
    
}
