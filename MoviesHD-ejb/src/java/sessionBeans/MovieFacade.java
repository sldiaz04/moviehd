/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionBeans;

import entities.Movie;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import singletons.SingletonStatistics;

/**
 *
 * @author sldia
 */
@Stateless
public class MovieFacade extends AbstractFacade<Movie> {

    @EJB
    private SingletonStatistics singletonStatistics;

    @PersistenceContext(unitName = "MoviesHD-ejbPU")
    private EntityManager em;
    
    
    private static Logger LOGGER;
    
    @PostConstruct
    void postConstruct() {
        LOGGER = LogManager.getLogger("singleton.stateless");
        LOGGER.info(this);
    }

    @PreDestroy
    void preDestroy() {
        LOGGER.info(this);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MovieFacade() {
        super(Movie.class);
    }
    
    /*
        Using NamedQuerys
     */
    public List<Movie> findMoviesByGenre(String genre, String user) {
        LOGGER.info(user);
        singletonStatistics.addComponetsPerUser(user);
        singletonStatistics.addVisitsPerPagesAndComponents(MovieFacade.class.getName());
        
        Query query = em.createQuery("SELECT m FROM Movie m WHERE m.genre LIKE :genre");
        query.setParameter("genre", "%" + genre + "%");
        return query.getResultList();
    }
}
