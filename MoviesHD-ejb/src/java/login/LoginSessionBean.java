/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import entities.Client;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sessionBeans.ClientFacade;
import singletons.SingletonStatistics;

/**
 *
 * @author sldia
 */
@Stateless
@LocalBean
public class LoginSessionBean {

    @EJB
    private SingletonStatistics singletonStatistics;

    @EJB
    private ClientFacade clientFacade;
    
    private static Logger LOGGER;
    
    @PostConstruct
    void postConstruct() {
        LOGGER = LogManager.getLogger("singleton.stateless");
        LOGGER.info(this);
    }
    
    @PreDestroy
    void preDestroy() {
        LOGGER.info(this);
    }
    
    public Client login(String email, String password, String sessionId){
        Client client = clientFacade.getClientByEmail(email);
        String userName = sessionId;
        if (client != null) {
            userName = client.getName() + " " + client.getLastname();
        }
        // Logs
        LOGGER.info(userName);
        singletonStatistics.incremetUserLoggin();
        singletonStatistics.addComponetsPerUser(userName);
        singletonStatistics.addVisitsPerPagesAndComponents(LoginSessionBean.class.getName());
        
        // Si no se encuentra el usuario, o si no coincide su clave
        if (client == null || !client.getPassword().equals(DigestUtils.sha1Hex(password))) {
            return null;
        }
        return client;
    }
    
}
