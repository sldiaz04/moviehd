/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import entities.Client;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityExistsException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sessionBeans.ClientFacade;
import singletons.SingletonStatistics;

/**
 *
 * @author sldia
 */
@Stateless
@LocalBean
public class RegisterSessionBean {

    @EJB
    private SingletonStatistics singletonStatistics;

    @EJB
    private ClientFacade clientFacade;
    
    private static Logger LOGGER;
    
    @PostConstruct
    void postConstruct() {
        LOGGER = LogManager.getLogger("singleton.stateless");
        LOGGER.info(this);
    }

    @PreDestroy
    void preDestroy() {
        LOGGER.info(this);
    }

    public boolean register(Client client) {
        Client user = clientFacade.getClientByEmail(client.getEmail());
        
        // Logs
        singletonStatistics.addComponetsPerUser(client.getName() + " " + client.getLastname());
        singletonStatistics.addVisitsPerPagesAndComponents(RegisterSessionBean.class.getName());
        LOGGER.info(client.getName() + " " + client.getLastname());
        
        if(user != null){
            return false;
        }
        try {
            clientFacade.create(client);
        } catch (EntityExistsException | IllegalArgumentException e) {
            return false;
        }
        return true;
    }
    
    public boolean updateClientData(Client client) {
        // Logs
        LOGGER.info(client.getName() + " " + client.getLastname());
        singletonStatistics.addComponetsPerUser(client.getName() + " " + client.getLastname());
        singletonStatistics.addVisitsPerPagesAndComponents(RegisterSessionBean.class.getName());
        
        try {
            clientFacade.edit(client);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }
}
