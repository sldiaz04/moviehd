/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cart;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.LocalBean;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Remove;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import singletons.SingletonStatistics;

/**
 *
 * @author sldia
 */
@Stateful
@LocalBean
public class CartSessionBean implements Serializable{

    @EJB
    private SingletonStatistics singletonStatistics;

    private static Logger LOGGER; 
    private List<CartLine> cart;

    @PostConstruct
    void postConstruct() {
        cart = new ArrayList<>();
        LOGGER = LogManager.getLogger("singleton.stateful");
        LOGGER.info(this);
    }

    @PrePassivate
    void prePassivate(){
        LOGGER.info(this);
    }
    
    @PostActivate
    void postActivate(){
        LOGGER.info(this);
    }
    
    @Remove
    public void remove(){
        LOGGER.info(this);
    }
    
    @PreDestroy
    void preDestroy(){
        LOGGER.info(this);
    }
    
    public void addMovie(CartLine cartLine, String user) {
        if (cart.contains(cartLine)) {
            int index = cart.indexOf(cartLine);
            int amount = cart.get(index).getAmount();
            cart.remove(cartLine);
            cartLine.setAmount(amount + 1);
            cart.add(index, cartLine);
        } else {
            cart.add(cartLine);
        }
        
        // Logs
        LOGGER.info(user);
        singletonStatistics.addComponetsPerUser(user);
        singletonStatistics.addVisitsPerPagesAndComponents(CartSessionBean.class.getName());
    }

    public  void addMovies(Collection CarList){
        cart.addAll(CarList);
    }
    
    public void remoMovie(CartLine cartLine, String user) {
        int index = cart.indexOf(cartLine);
        CartLine lineSessionBean = cart.get(index);
        cart.remove(cartLine);
        int amount = lineSessionBean.getAmount();
        if(amount > 1){
            lineSessionBean.setAmount(amount - 1);
            cart.add(index, lineSessionBean);
        }
        
        // Logs
        LOGGER.info(user);
        singletonStatistics.addComponetsPerUser(user);
        singletonStatistics.addVisitsPerPagesAndComponents(CartSessionBean.class.getName());
    }
    
    public void deleteMovie(CartLine cartLine, String user){
        cart.remove(cartLine);
        
        // Logs
        LOGGER.info(user);
        singletonStatistics.addComponetsPerUser(user);
        singletonStatistics.addVisitsPerPagesAndComponents(CartSessionBean.class.getName());
    }

    public List<CartLine> getItems() {
        singletonStatistics.addVisitsPerPagesAndComponents(CartSessionBean.class.getName());
        return cart;
    }

    public int getItemsCount() {
        singletonStatistics.addVisitsPerPagesAndComponents(CartSessionBean.class.getName());
        return cart.size();
    }
    
    public double getTotalPrice(){
        singletonStatistics.addVisitsPerPagesAndComponents(CartSessionBean.class.getName());
        double totalPrice = 0;
        totalPrice = cart.stream().map((cartLineSessionBean) -> cartLineSessionBean.getTotalPrice()).reduce(totalPrice, (accumulator, _item) -> accumulator + _item);
        BigDecimal bd = new BigDecimal(totalPrice);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
