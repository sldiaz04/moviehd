/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cart;

import entities.Movie;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import stratergy.DiscountStrategy;

/**
 *
 * @author sldia
 */
public class CartLine implements Serializable{
    private Movie movie;
    private int amount;
    private double discount;
    private DiscountStrategy discountStrategy;

    public CartLine(Movie movie, DiscountStrategy discountStrategy) {
        this.movie = movie;
        this.amount = 1;
        this.discountStrategy = discountStrategy;
        discount = 0.0;
    }

    public CartLine(Movie movie) {
        this.movie = movie;
        this.amount = 1;
    }
    public void applyDiscount(){
        discountStrategy.applyDiscount(this);
    }
    
    public void setMovie(Movie movie){
        this.movie = movie;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getDiscount() {
        BigDecimal bd = new BigDecimal(amount * discount);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotalPrice() {
        BigDecimal bd = new BigDecimal(amount * movie.getPrice());
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return   bd.doubleValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CartLine) {
            CartLine clsb = (CartLine) obj;
            return this.movie.equals(clsb.getMovie());
        }
        return false;
    }
    
    
}
