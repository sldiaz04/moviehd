/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.model;

import entities.Client;
import java.io.Serializable;

/**
 *
 * @author sldia
 */
public class Comment implements Serializable{
    private Client user;
    private String message;

    public Comment(Client user, String message) {
        this.user = user;
        this.message = message;
    }

    public Client getUser() {
        return user;
    }

    public void setUser(Client user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
}
