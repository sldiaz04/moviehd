/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.mdb;

import chat.model.Comment;
import chat.singleton.ChatSingleton;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 *
 * @author sldia
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jmsDemo/sergioDest")
    ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class ChatMessageDrivenBean implements MessageListener {

    @EJB
    private ChatSingleton chatSingleton;
    
    public ChatMessageDrivenBean() {
    }
    
    @Override
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
            ObjectMessage om = (ObjectMessage) message;
            
            try {
                Comment comment = (Comment) om.getObject();
                chatSingleton.addComment(comment);
            } catch (JMSException ex) {
                Logger.getLogger(ChatMessageDrivenBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
