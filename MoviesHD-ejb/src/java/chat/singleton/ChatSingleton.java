/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat.singleton;

import chat.model.Comment;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import singletons.SingletonStatistics;

/**
 *
 * @author sldia
 */
@Singleton
@LocalBean
public class ChatSingleton {

    @EJB
    private SingletonStatistics singletonStatistics;
    private static Logger LOGGER;
    private List<Comment> comments;

    @PostConstruct
    void postConstruct() {
        comments = new ArrayList<>();
        LOGGER = LogManager.getLogger("singleton");
        LOGGER.info(this);
    }

    @PreDestroy
    void  preDestroy(){
        LOGGER.info(this);
    }
    
    public void addComment(Comment comment) {
        comments.add(comment);
        
        // Logs
        LOGGER.info(this);
        singletonStatistics.addComponetsPerUser(comment.getUser().getName() + " " + comment.getUser().getLastname());
        singletonStatistics.addVisitsPerPagesAndComponents(ChatSingleton.class.getName());
    }

    public List<Comment> getComments() {
        return comments;
    }
}
