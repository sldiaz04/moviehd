/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testng;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 *
 * @author sldia
 */
public class StartTestNG {

    WebDriver driver;

    @BeforeTest
    public void invokeBrowser() {
        try {
            System.setProperty("webdriver.chrome.driver",
                    "..\\driver\\chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().deleteAllCookies();
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

            driver.get("http://localhost:8080/MoviesHD-war");
        } catch (Exception e) {
        }
    }

    @Test(priority = 0)
    public void showMovie() {
        driver.navigate().to("http://localhost:8080/MoviesHD-war/FrontController?command=ProductDetailCommand&image=3");
        Assert.assertEquals("http://localhost:8080/MoviesHD-war/FrontController?command=ProductDetailCommand&image=3", driver.getCurrentUrl());
    }

    @Test(priority = 1)
    public void addMovieToCart() {
        try {
            Thread.sleep(1000);
            driver.findElement(By.linkText("8.55 €")).click();
            Assert.assertEquals("http://localhost:8080/MoviesHD-war/index.jsp", driver.getCurrentUrl());
        } catch (InterruptedException ex) {
            Logger.getLogger(StartTestNG.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test(priority = 2)
    public void showCartDetail() {
        try {
            Thread.sleep(1000);
            driver.findElement(By.xpath("//a[@href='FrontController?command=CartDetailCommand']")).click();
            Assert.assertEquals("http://localhost:8080/MoviesHD-war/FrontController?command=CartDetailCommand", driver.getCurrentUrl());
        } catch (InterruptedException ex) {
            Logger.getLogger(StartTestNG.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test(priority = 3)
    public void payCart() {
        try {
            Thread.sleep(1000);
            driver.findElement(By.className("btn-block")).click();
        } catch (InterruptedException ex) {
            Logger.getLogger(StartTestNG.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test(priority = 4)
    public void login() {
        try {
            Thread.sleep(1000);
            driver.findElement(By.name("loginEmail")).sendKeys("sergio@gmail.com");
            Thread.sleep(1000);
            driver.findElement(By.name("loginPassword")).sendKeys("1234");
            Thread.sleep(1000);
            driver.findElement(By.xpath("//button[@type='submit' and text()='Acceder']")).click();
            Assert.assertEquals("http://localhost:8080/MoviesHD-war/FrontController?command=LoginCommand", driver.getCurrentUrl());
        } catch (InterruptedException ex) {
            Logger.getLogger(StartTestNG.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test(priority = 5)
    public void navegateOnUserAccount() {
        try {
            Thread.sleep(1000);
            driver.findElement(By.id("v-pills-home-tab")).click();
            Thread.sleep(1000);
            driver.findElement(By.id("v-pills-profile-tab")).click();
            Thread.sleep(1000);
            driver.findElement(By.id("v-pills-settings-tab")).click();
            Assert.assertEquals("http://localhost:8080/MoviesHD-war/FrontController?command=LogoutCommand", driver.getCurrentUrl());
        } catch (InterruptedException ex) {
            Logger.getLogger(StartTestNG.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test(priority = 6)
    public void register() {
        driver.findElement(By.id("nav-profile-tab")).click();
        driver.findElement(By.name("UserNameRegister")).sendKeys("Test");
        driver.findElement(By.name("LastNameRegister")).sendKeys("Test Lastname");
        driver.findElement(By.name("EmailRegister")).sendKeys("test@gmail.com");
        driver.findElement(By.name("PasswordRegister")).sendKeys("test1234");
        driver.findElement(By.name("AddressRegister")).sendKeys("Test address");
        driver.findElement(By.name("CityRegister")).sendKeys("Test city");
        driver.findElement(By.name("StateRegister")).sendKeys("Test state");
        driver.findElement(By.name("ZipCodeRegister")).sendKeys("00000");
        driver.findElement(By.name("PhoneRegister")).sendKeys("123456789");
        driver.findElement(By.xpath("//div[@class='custom-control custom-checkbox my-1 mr-sm-2']/label")).click();
        driver.findElement(By.xpath("//button[@type='submit' and text()='Registarse']")).click();
        Assert.assertEquals("http://localhost:8080/MoviesHD-war/FrontController?command=RegisterUserCommand", driver.getCurrentUrl());
    }
    
    @AfterTest
    public void closeBrowser(){
        driver.quit();
    }
}
