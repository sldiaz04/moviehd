<%@ include file="/includes/header.jsp" %>
<br><br>

<%
    SingletonStatistics singletonStatistics = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/SingletonStatistics!singletons.SingletonStatistics");
    singletonStatistics.addVisitsPerPagesAndComponents("loginAndRegister.jsp");
    Client user = (Client) session.getAttribute("client");
    String userName = session.getId();

    if (user != null) {
        userName = user.getName() + " " + user.getLastname();
    }
    singletonStatistics.addComponetsPerUser(userName);
%>

<div class="container mt-4">
    <div class="row justify-content-md-center">
        <div class="col">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Acceso</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Registro</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <!-- Login Form -->
                    <form action="FrontController?command=LoginCommand" method="POST" class="needs-validation" novalidate>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationCustomUsernameLogin">Usuario</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                                    </div>
                                    <input type="text" class="form-control" name="loginEmail" id="validationCustomUsernameLogin" placeholder="Usuario" aria-describedby="inputGroupPrepend" required>
                                    <div class="invalid-feedback">
                                        Por favor, elija un correo electr�nico.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationCustomPasswordLogin">Contrase�a</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-key"></i></span>
                                    </div>
                                    <input type="password" class="form-control" name="loginPassword" id="validationCustomPasswordLogin" placeholder="Contrase�a" aria-describedby="inputGroupPrepend" required>
                                    <div class="invalid-feedback">
                                        Por favor, elija una contrase�a.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <button class="btn btn-primary" type="submit">Acceder</button>
                        </div>
                    </form>
                    <!-- End Login Form -->
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <!-- Register Form-->
                    <form action="FrontController?command=RegisterUserCommand" method="POST" class="needs-validation" novalidate>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationCustomUserNameRegister">Nombre</label>
                                <input type="text" class="form-control" name="UserNameRegister" id="validationCustomUserNameRegister" placeholder="Nombre" value="Sergio" required>
                                <div class="valid-feedback">
                                    �Se ve bien!
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationCustomLastNameRegister">Apellidos</label>
                                <input type="text" class="form-control" name="LastNameRegister" id="validationCustomLastNameRegister" placeholder="Apellidos" value="L�pez D�az" required>
                                <div class="valid-feedback">
                                    �Se ve bien!
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationCustomEmailRegister">Usuario</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                                    </div>
                                    <input type="text" class="form-control" name="EmailRegister" id="validationCustomEmailRegister" placeholder="Usuario" aria-describedby="inputGroupPrepend" required>
                                    <div class="invalid-feedback">
                                        Por favor, elija un correo electr�nico.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationCustomPasswordRegister">Contrase�a</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-key"></i></span>
                                    </div>
                                    <input type="password" class="form-control" name="PasswordRegister" id="validationCustomPasswordRegister" placeholder="Contrase�a" aria-describedby="inputGroupPrepend" required>
                                    <div class="invalid-feedback">
                                        Por favor, elija una contrase�a.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationCustomAddressRegister">Direcci�n</label>
                                <input type="text" class="form-control" name="AddressRegister" id="validationCustomAddressRegister" placeholder="Direcci�n" required>
                                <div class="invalid-feedback">
                                    Por favor, proporcione una direcci�n v�lida.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationCustomCityRegister">Ciudad</label>
                                <input type="text" class="form-control" name="CityRegister" id="validationCustomCityRegister" placeholder="Ciudad" required>
                                <div class="invalid-feedback">
                                    Por favor, proporciona una ciudad v�lida.
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationCustomStateRegister">Provincia</label>
                                <input type="text" class="form-control" name="StateRegister" id="validationCustomStateRegister" placeholder="Provincia" required>
                                <div class="invalid-feedback">
                                    Por favor, proporcione un estado v�lido.
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationCustomZipCodeRegister">C�digo Postal</label>
                                <input type="text" class="form-control" name="ZipCodeRegister" id="validationCustomZipCodeRegister" placeholder="C�digo Postal" required>
                                <div class="invalid-feedback">
                                    Proporcione un c�digo postal v�lido.
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="validationCustomPhoneRegister">Tel�fono</label>
                                <input type="text" class="form-control" name="PhoneRegister" id="validationCustomPhoneRegister" placeholder="Tel�fono" required>
                                <div class="invalid-feedback">
                                    Proporcione un c�digo postal v�lido.
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                <input class="custom-control-input" type="checkbox" value="" id="invalidCheck" required>
                                <label class="custom-control-label" for="invalidCheck">
                                    Acepta los t�rminos y condiciones
                                </label>
                                <div class="invalid-feedback">
                                    Debes aceptar antes de enviar.
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Registarse</button>
                    </form>
                    <!-- End Register Form-->
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file = "/includes/footer.jsp" %>
