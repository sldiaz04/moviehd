<%@page import="entities.Movie"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="sessionBeans.MovieFacade"%>
<%@ include file = "includes/header.jsp" %>

<%
    MovieFacade movieFacade = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/MovieFacade!sessionBeans.MovieFacade");
    List<Movie> movies = movieFacade.findAll();
    pageContext.setAttribute("movies", movies);

    // Logs
    SingletonStatistics singletonStatistics = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/SingletonStatistics!singletons.SingletonStatistics");
    singletonStatistics.addVisitsPerPagesAndComponents("index.jsp");
    Client user = (Client) session.getAttribute("client");
    String userName = session.getId();

    if (user != null) {
        userName = user.getName() + " " + user.getLastname();
    }
    singletonStatistics.addComponetsPerUser(userName);
%>

<div id="slideShow" class="w3-content w3-display-container" style="max-width:100%">
    <img class="mySlides w3-animate-right" src="images/panoramica1.jpg" style="width:100%; height: 500px">
    <img class="mySlides w3-animate-right" src="images/panoramica2.gif" style="width:100%; height: 500px">
    <img class="mySlides w3-animate-right" src="images/panoramica3.jpg" style="width:100%; height: 500px">
    <div class="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle" style="width:100%">
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(0)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(1)"></span>
        <span class="w3-badge demo w3-border w3-transparent w3-hover-white" onclick="currentDiv(2)"></span>
    </div>
</div>

<div class="container mt-4">
    <%@ include file = "/includes/movies.jsp" %>
</div>

<%@ include file = "includes/footer.jsp" %>