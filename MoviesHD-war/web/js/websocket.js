var socket = new WebSocket("ws://192.168.1.45:8080/MoviesHD-war/actions");
socket.onmessage = onMessage;

function onMessage(event) {
    var comment = JSON.parse(event.data);
    if (comment.action === "add") {
        printComment(comment);
    }
}

socket.onopen = function (){
    console.log('Connected');
};

socket.onclose = function () {
    console.log('Disconnected');
};

/*socket.onerror = function (event) {
    console.log('Error occurred: ' + JSON.stringify(event));
};*/

function addComment(userName, message) {
    var CommentAction = {
        action: "add",
        userName: userName,
        message: message
    };
    socket.send(JSON.stringify(CommentAction));
}

function printComment(comment) {
    // Comment
    var ul = document.getElementById("comments-list");
    var li = document.createElement("li");

    var mainDiv = document.createElement("div");
    mainDiv.setAttribute("class", "comment-main-level");

    var avatarDiv = document.createElement("div");
    avatarDiv.setAttribute("class", "comment-avatar");

    var imageAvatar = document.createElement("img");
    if (comment.userName === 'Anonymous') {
        imageAvatar.setAttribute("src", "images/user-anonymous.png");
    } else {
        imageAvatar.setAttribute("src", "images/user-laptop.png");
    }

    imageAvatar.setAttribute("alt", "Imagen de perfil");
    avatarDiv.appendChild(imageAvatar);

    var commentDiv = document.createElement("div");
    commentDiv.setAttribute("class", "comment-box");

    var commentHeadDiv = document.createElement("div");
    commentHeadDiv.setAttribute("class", "comment-head");

    var h6 = document.createElement("h6");
    h6.setAttribute("class", "comment-name by-author");
    h6.innerHTML = comment.userName;
    commentHeadDiv.appendChild(h6);

    var commentContentDiv = document.createElement("div");
    commentContentDiv.setAttribute("class", "comment-content");
    commentContentDiv.innerHTML = comment.message;
    commentDiv.appendChild(commentHeadDiv);
    commentDiv.appendChild(commentContentDiv);

    mainDiv.appendChild(avatarDiv);
    mainDiv.appendChild(commentDiv);
    li.appendChild(mainDiv);
    ul.appendChild(li);
}

function formSubmit() {
    var userName = document.getElementById("username").value;
    var message = document.getElementById("message").value;
    addComment(userName, message);
    document.getElementById("message").value = '';
    document.getElementById("message").focus();
}