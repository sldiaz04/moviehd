var myIndex = 0;
var ctime = 0;
carousel();

function currentDiv(n) {
    myIndex = n;
    clearTimeout(ctime);
    carousel();
}

function carousel() {
    let i;
    let x = document.getElementsByClassName("mySlides");
    let dots = document.getElementsByClassName("demo");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    myIndex++;
    if (myIndex > x.length) {
        myIndex = 1;
    }
    x[myIndex - 1].style.display = "block";

    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" w3-white", "");
    }
    dots[myIndex - 1].className += " w3-white";
    ctime = setTimeout(carousel, 3000); // Change image every 2 seconds
}

function myOpacity() {
    let header = document.getElementById("myopacity");
    let slideShow = document.getElementById("slideShow");
    if (slideShow === null) {
        header.className += " w3-theme-d4";
    } else {
        header.style.background = "rgba(46, 64, 83,0.5)";
    }
}

function faqFunction(id) {
    let x = document.getElementById(id);
    if (x.className.indexOf("w3-show") === -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}

function stopVideoModal() {
    let elemento = document.querySelector('#videoframe');
    let src = elemento.getAttribute('src');
    elemento.setAttribute('src', '');
    elemento.setAttribute('src', src);
}

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        let forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();


