<%@ include file = "includes/header.jsp" %>
<br><br><br><br>

<%
    SingletonStatistics singletonStatistics = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/SingletonStatistics!singletons.SingletonStatistics");
    singletonStatistics.addVisitsPerPagesAndComponents("statistics.jsp");
    Client user = (Client) session.getAttribute("client");
    String userName = session.getId();

    if (user != null) {
        userName = user.getName() + " " + user.getLastname();
    }
    singletonStatistics.addComponetsPerUser(userName);
%>

<div class="container">
    <c:if test="${statistics.size() != 0}">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="FrontController?command=StatisticsCommand&previous=${currentpage}">Anterior</a>
            </li>
            <c:forEach var="currentPage" begin="1" end="${totalregister}">
                <li class="page-item"><a class="page-link" href="FrontController?command=StatisticsCommand&currentpage=${currentPage}">${currentPage}</a></li>
                </c:forEach>
            <li class="page-item">
                <a class="page-link" href="FrontController?command=StatisticsCommand&next=${currentpage}">Siguiente</a>
            </li>
        </ul>
        <table class="table table-striped table-hover table-responsive-sm">
            <thead class="w3-theme-l1">
                <tr>
                    <th>ID</th>
                    <th>LEVEL</th>
                    <th>LOGGERNAME</th>
                    <th>MESSAGE</th>
                    <th>SOURCE</th>
                    <th>TIMEMILLIS</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="statistic" items="${statistics}">
                    <tr>
                        <td>${statistic.id}</td>
                        <td>${statistic.level}</td>
                        <td>${statistic.logerName}</td>
                        <td>${statistic.message}</td>
                        <td>${statistic.source}</td>
                        <td>${statistic.timeMillis}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <a class="page-link" href="FrontController?command=StatisticsCommand&previous=${currentpage}">Anterior</a>
            </li>
            <c:forEach var="currentPage" begin="1" end="${totalregister}">
                <li class="page-item"><a class="page-link" href="FrontController?command=StatisticsCommand&currentpage=${currentPage}">${currentPage}</a></li>
                </c:forEach>
            <li class="page-item">
                <a class="page-link" href="FrontController?command=StatisticsCommand&next=${currentpage}">Siguiente</a>
            </li>
        </ul>
    </c:if>
</div>

<%@ include file = "includes/footer.jsp" %>