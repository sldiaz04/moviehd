<%-- 
    Document   : unknownPage
    Created on : 08-feb-2018, 16:55:48
    Author     : sldia
--%>

<%@ include file = "includes/header.jsp" %>

<%
    SingletonStatistics singletonStatistics = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/SingletonStatistics!singletons.SingletonStatistics");
    singletonStatistics.addVisitsPerPagesAndComponents("unknown.jsp");
    Client user = (Client) session.getAttribute("client");
    String userName = session.getId();

    if (user != null) {
        userName = user.getName() + " " + user.getLastname();
    }
    singletonStatistics.addComponetsPerUser(userName);
%>

<div class="w3-container w3-display-middle">
    <img src="images/unknown.jpg" alt="Unknown command">    
</div>

<%@ include file = "includes/footer.jsp" %>
