<%@ include file = "includes/header.jsp" %>
<br/><br/>

<%
    SingletonStatistics singletonStatistics = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/SingletonStatistics!singletons.SingletonStatistics");
    singletonStatistics.addVisitsPerPagesAndComponents("productDetails.jsp");
    Client user = (Client) session.getAttribute("client");
    String userName = session.getId();

    if (user != null) {
        userName = user.getName() + " " + user.getLastname();
    }
    singletonStatistics.addComponetsPerUser(userName);
%>

<div class="container">
    <div class="row mt-4 w3-theme-l5 py-2">
        <div class="col-6 col-sm-3 col-md-2 mb-5">
            <img src="images/${movie.image}" class="w3-image" style="width:100%; height: 100%;">
            <a href="FrontController?command=AddMovieCommand&id=${movie.id}" class="w3-button w3-block w3-yellow">${movie.price} &euro;</a>
        </div>
        <div class="col-12 col-sm-9 col-md-10">
            <h2 class="d-inline-block mr-4">${movie.title}</h2>

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary h2" data-toggle="modal" data-target="#exampleModalCenter">
                <i class="fas fa-film"></i> Trailer
            </button>
            <p>${movie.description}</p>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document" id="exampleModalCenter">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title h4">${movie.title}</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="stopVideoModal()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe id="videoframe" src="${movie.trailer}" frameborder="0" allowfullscreen></iframe> 
            </div>
        </div>
    </div>
</div>

<%@ include file = "includes/footer.jsp" %>