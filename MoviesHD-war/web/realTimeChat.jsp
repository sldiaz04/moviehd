<%@ include file = "includes/header.jsp" %>
<br><br>

<%
    Client user = (Client) session.getAttribute("client");
    String userName = "Anonymous";
    if (user != null) {
        userName = user.getName();
    }
    pageContext.setAttribute("userName", userName);
%>
<!-- Contenedor Principal -->
<div class="comments-container">
    <ul id="comments-list" class="comments-list"></ul>
    <div id="addCommentForm" class="chat-form">
        <form class="input-group" onsubmit="formSubmit(); return false;">
            <input id="message" name="message" class="form-control" placeholder="Escriba un comentario" style="z-index: 0;" autofocus>
            <input id="username" type="hidden" name="userName" value="${userName}">
            <div class="input-group-append">
                <button class="btn btn-outline-info" style="z-index: 0;">Enviar</button>
            </div>
        </form>
    </div>
</div>
            
<!-- Own JavaScript -->
<script src="js/websocket.js"></script>

<%@ include file = "includes/footer.jsp" %>