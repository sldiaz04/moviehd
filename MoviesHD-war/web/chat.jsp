<%@page import="chat.model.Comment"%>
<%@page import="chat.singleton.ChatSingleton"%>
<%@ include file = "includes/header.jsp" %>
<br><br>

<%
    // Set refresh, autoload time as 10 seconds
    response.setIntHeader("Refresh", 15);

    ChatSingleton chatSingleton = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/ChatSingleton!chat.singleton.ChatSingleton");
    List<Comment> comments = chatSingleton.getComments();
    pageContext.setAttribute("chat", comments);

    // Logs
    SingletonStatistics singletonStatistics = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/SingletonStatistics!singletons.SingletonStatistics");
    singletonStatistics.addVisitsPerPagesAndComponents("chat.jsp");
    Client user = (Client) session.getAttribute("client");
    String userName = session.getId();
    String userId = "";

    if (user != null) {
        userName = user.getName() + " " + user.getLastname();
        userId = user.getId().toString();
    } else {
        userId = "Anonymous";
    }
    singletonStatistics.addComponetsPerUser(userName);
    pageContext.setAttribute("userId", userId);

%>

<!-- Background video-->
<%-- 
<video src="resources/AVENGERS_INFINITY_WAR.MP4" autoplay loop muted poster="resources/infinity_war.jpg"></video>
--%>

<!-- Contenedor Principal -->
<div class="comments-container">
    <ul id="comments-list" class="comments-list">
        <c:forEach var="comment" items="${chat}">
            <li>
                <div class="comment-main-level">
                    <!-- Avatar -->
                    <div class="comment-avatar"><img src="images/${comment.user.imageProfile}" alt="Imagen de perfil"></div>
                    <!-- Contenedor del Comentario -->
                    <div class="comment-box">
                        <div class="comment-head">
                            <h6 class="comment-name by-author">${comment.user.name}</h6>
                            <span>hace 20 minutos</span>
                            <i class="fas fa-reply"></i>
                            <i class="fas fa-heart"></i>
                        </div>
                        <div class="comment-content">
                            ${comment.message}
                        </div>
                    </div>
                </div>
            </li>
        </c:forEach>
    </ul>
    <form action="MessageDrivenServlet" method="post" class="chat-form">
        <div class="input-group">
            <input type="text" name="comment" class="form-control" placeholder="Escriba un comentario" style="z-index: 0;" autofocus>
            <input type="hidden" name="userId" value="${userId}">
            <div class="input-group-append">
                <button class="btn btn-outline-info" type="submit" style="z-index: 0;">Enviar</button>
            </div>
        </div>
    </form>
</div>

<%@ include file = "includes/footer.jsp" %>