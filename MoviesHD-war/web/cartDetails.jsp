<%-- 
    Document   : cartDetails
    Created on : 22-mar-2018, 12:06:59
    Author     : sldia
--%>
<%@ include file = "includes/header.jsp" %>
<br><br>

<%
    SingletonStatistics singletonStatistics = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/SingletonStatistics!singletons.SingletonStatistics");
    singletonStatistics.addVisitsPerPagesAndComponents("cartDetails.jsp");
    Client user = (Client) session.getAttribute("client");
    String userName = session.getId();

    if (user != null) {
        userName = user.getName() + " " + user.getLastname();
    }
    singletonStatistics.addComponetsPerUser(userName);
%>

<c:if test="${cart.getItemsCount() == 0}">
    <img src="images/empty_cart.png" alt="Empty cart" class="w3-display-middle">
</c:if>

<c:if test="${cart.getItemsCount() != 0}">
    <div class="container mt-4">
        <h1 class="text-center font-italic">Carro de la compra</h1>
        <hr>
        <div class="row">
            <section class="col-md-9 table-responsive mb-3">
                <table class="table">
                    <thead class="w3-theme-l1">
                        <tr>
                            <th scope="col">PELÍCULA</th>
                            <th scope="col"></th>
                            <th scope="col">PRECIO</th>
                            <th scope="col" style="min-width: 122px">UNIDADES</th>
                            <th scope="col">TOTAL</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="item" items="${cart.getItems()}">
                            <tr>
                                <td><a href="FrontController?command=ProductDetailCommand&image=${item.movie.id}"><img src="images/${item.movie.image}" style="width: 60px; height: 110px;"></a></td>
                                <td class="align-middle">
                                    <h5>${item.movie.title}</h5>
                                </td>
                                <td class="align-middle">${item.movie.price}</td>
                                <td class="align-middle" style="min-width: 122px">
                                    <div class="input-group">
                                        <form action="FrontController?command=RemoveMovieCommand" method="POST" class="input-group-prepend">
                                            <button class="btn btn-outline-secondary" type="submit" style="width: 10px;">-</button>
                                            <input type="hidden" name="id" value="${item.movie.id}">
                                            <input type="hidden" name="deleteitem" value="${item.amount - 1}">
                                        </form>
                                        <input type="text" value="${item.amount}" size="1" style="text-align: center" readonly="true">
                                        <form action="FrontController?command=AddMovieCommand" method="POST" class="input-group-append">
                                            <input type="hidden" name="id" value="${item.movie.id}">
                                            <input type="hidden" name="additem" value="${item.amount - 1}">
                                            <button class="btn btn-outline-secondary" type="submit" style="width: 10px;">+</button>
                                        </form>
                                    </div>
                                </td>
                                <td class="align-middle">${item.getTotalPrice()}</td>
                                <td class="align-middle">
                                    <form action="FrontController?command=RemoveMovieCommand" method="POST">
                                        <input type="hidden" name="id" value="${item.movie.id}">
                                        <input type="hidden" name="fulldelete" value="${item.movie.id}">
                                        <button type="submit" class="btn btn-outline-danger"> <i class="fas fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </section>
            <section class="col-md-3">
                <div class="card text-center">
                    <div class="card-header w3-theme-l1">
                        DETALLES DEL PEDIDO
                    </div>
                    <div class="card-body">
                        <p class="card-text">Las mejores películas y de buena calidad.</p>
                    </div>
                    <h5 class="card-title">TOTAL ${cart.getTotalPrice()}</h5>
                </div>
                <hr>
                <form action="FrontController?command=PaymentCartCommand" method="POST">
                    <input class="btn btn-outline-primary btn-block" type="submit" value="REALIZAR PEDIDO">
                </form>
            </section>
        </div>
    </div>
</c:if>
<%@ include file = "includes/footer.jsp" %>