<%-- 
    Document   : faq
    Created on : 26-feb-2018, 9:14:02
    Author     : sergio
--%>

<%@page import="entities.Faq"%>
<%@page import="sessionBeans.FaqFacade"%>
<%@ include file = "includes/header.jsp" %>
<br><br>
<%
    // Logs
    SingletonStatistics singletonStatistics = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/SingletonStatistics!singletons.SingletonStatistics");
    singletonStatistics.addVisitsPerPagesAndComponents("faq.jsp");
    Client user = (Client) session.getAttribute("client");
    String userName = session.getId();

    if (user != null) {
        userName = user.getName() + " " + user.getLastname();
    }
    singletonStatistics.addComponetsPerUser(userName);

    // FAQs
    FaqFacade faqFacade = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/FaqFacade");
    List<Faq> faqs = faqFacade.getAllFAQs();
    pageContext.setAttribute("faqs", faqs);
%>

<div class="w3-container w3-section w3-animate-top" style="margin-left:20%;margin-right:20%">
    <h1 class="text-center font-italic">Preguntas frecuentes</h1>
    <ol class="w3-ul w3-theme-l4 w3-hoverable w3-round-large" style="width:100%;">
        <c:forEach var="faq" items="${faqs}">
            <li class="w3-padding-large" onclick="faqFunction('faq${faq.id}')">
                ${faq.question}
            </li>
            <div id="faq${faq.id}" class="w3-hide w3-container w3-light-grey">
                <p>
                    ${faq.answer}
                </p>
            </div>
        </c:forEach>
    </ol>
</div>

<%@ include file = "includes/footer.jsp" %>
