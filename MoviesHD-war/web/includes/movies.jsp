<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <c:forEach var="movie" items="${movies}">
        <div class="col-6 col-sm-4 col-md-2 mb-4">
            <div class="w3-card-4 w3-dropdown-hover">
                <img src="images/${movie.image}" class="w3-image" style="width:100%; height: 250px;">
                <div class="w3-dropdown-content w3-card-4 w3-display-topleft" style="width:250px;">
                    <a href="FrontController?command=ProductDetailCommand&image=${movie.id}"><img src="images/${movie.image}" alt="${movie.title}" style="width:100%"></a>
                    <a href="FrontController?command=AddMovieCommand&id=${movie.id}" class="w3-button w3-block w3-yellow">${movie.price} &euro;</a>
                </div>
            </div>
        </div>
    </c:forEach>
</div>
