<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entities.Client"%>
<%@page import="javax.naming.InitialContext"%>
<%@page import="singletons.SingletonStatistics"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Movies HD</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- W3school CSS -->
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-indigo.css">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Icons -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>

        <!-- Google Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>

        <!-- Own Styles -->
        <link href="css/chat.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <link href="css/header.css" rel="stylesheet">
    </head>

    <body onload="myOpacity()">
        <header class="w3-top">
            <nav class="navbar navbar-expand-lg" id="myopacity">
                <a href="FrontController?command=HomePageCommand" class="navbar-brand text-warning"><i class="fas fa-home"></i></a>
                <button class="navbar-toggler text-warning" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-warning" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Categor�as
                            </a>
                            <div class="dropdown-menu bg-warning" aria-labelledby="navbarDropdown">
                                <a href="FrontController?command=MovieCommand&genre=action" class="dropdown-item">Acci�n</a>
                                <a href="FrontController?command=MovieCommand&genre=animation" class="dropdown-item">Animaci�n</a>
                                <a href="FrontController?command=MovieCommand&genre=comedy" class="dropdown-item">Comedia</a>
                                <a href="FrontController?command=MovieCommand&genre=drama" class="dropdown-item">Drama</a>
                                <a href="FrontController?command=MovieCommand&genre=fantasy" class="dropdown-item">Fant�stico</a>
                                <a href="FrontController?command=MovieCommand&genre=intrigue" class="dropdown-item">Intriga</a>
                                <a href="FrontController?command=MovieCommand&genre=romantic" class="dropdown-item">Rom�ntica</a>
                                <a href="FrontController?command=MovieCommand&genre=horror" class="dropdown-item">Terror</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-warning" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Por a�o
                            </a>
                            <div class="dropdown-menu bg-warning" aria-labelledby="navbarDropdown">
                                <a href="#" class="dropdown-item">Pel�culas del 2018</a>
                                <a href="#" class="dropdown-item">Pel�culas del 2017</a>
                                <a href="#" class="dropdown-item">Pel�culas del 2016</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link text-warning">Espa�ol</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link text-warning">Subtituladas</a>
                        </li>
                        <li class="nav-item">
                            <a href="FrontController?command=FaqCommand" class="nav-link text-warning">Preguntas frecuentes</a>
                        </li>
                        <li class="nav-item">
                            <a href="FrontController?command=StatisticsCommand" class="nav-link text-warning">Estad�sticas</a>
                        </li>
                        <li class="nav-item">
                            <a href="FrontController?command=ChatCommand" class="nav-link text-warning"><i class="fas fa-comment-alt"></i></a>
                        </li>
                        <li class="nav-item">
                            <a href="FrontController?command=RealTimeChatCommand" class="nav-link text-warning"><i class="fas fa-comments"></i></a>
                        </li>
                    </ul>
                    <div class="nav navbar-nav ml-auto">
                        <a href="FrontController?command=LoginCommand" class="nav-link text-warning"><i class="fas fa-user-secret"></i></a>
                        <a href="FrontController?command=CartDetailCommand" class="nav-link text-warning"><i class="fas fa-shopping-cart"></i>
                            <span class="w3-badge w3-tiny w3-red">${empty cart ? '0': cart.getItemsCount()}</span>
                        </a>
                    </div>
                </div>
            </nav>
        </header>

        <script>
            function collapseFunction() {
                var x = document.getElementById("demo");
                if (x.className.indexOf("w3-show") === -1) {
                    x.className += " w3-show";
                } else {
                    x.className = x.className.replace(" w3-show", "");
                }
            }
        </script>