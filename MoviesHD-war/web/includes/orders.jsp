<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${invoces.size() == 0}">
    <div>
        <i class="fab fa-cc-visa fa-10x text-primary align-middle"></i>
        <i class="fab fa-cc-paypal fa-10x text-success align-middle"></i>
        <i class="fab fa-cc-mastercard fa-10x text-warning align-middle"></i>
    </div>
</c:if>
<c:if test="${invoces.size() != 0}">
    <c:forEach var="invoce" items="${invoces}">
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#${invoce.id}" aria-expanded="true" aria-controls="collapseOne">
                            <span>N� de pedido: ${invoce.id} | </span>
                            <span>Fecha: ${invoce.purchaseDate}</span>
                        </button>
                    </h5>
                </div>

                <div id="${invoce.id}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body  table-responsive-sm">
                        <table class="table">
                            <thead class="text-primary">
                                <tr>
                                    <th scope="col">PEL�CULA</th>
                                    <th scope="col"></th>
                                    <th scope="col">PRECIO</th>
                                    <th scope="col">UNIDADES</th>
                                    <th scope="col">DESCUENTO</th>
                                    <th scope="col">TOTAL</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="detail" items="${invoce.getDetailList()}">
                                    <tr>
                                        <td><a href="FrontController?command=ProductDetailCommand&image=${detail.idMovie.id}"><img src="images/${detail.idMovie.image}" style="width: 60px; height: 110px;"></a></td>
                                        <td class="align-middle">
                                            <h5 class="text-success">${detail.idMovie.title}</h5>
                                        </td>
                                        <td class="align-middle">${detail.idMovie.price}</td>
                                        <td class="align-middle">${detail.amount}</td>
                                        <td class="align-middle">${detail.discount}</td>
                                        <td class="align-middle">${detail.price}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6" class="text-right text-primary h4">${invoce.price}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
</c:if>