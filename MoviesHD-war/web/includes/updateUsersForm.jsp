<form action="FrontController?command=RegisterUserCommand" method="POST" class="needs-validation" novalidate>
    <div class="form-row">
        <div class="col-md-6 mb-3">
            <label for="validationCustomUserNameRegister">Nombre</label>
            <input type="text" class="form-control" name="UserNameRegister" id="validationCustomUserNameRegister" placeholder="Nombre" value="${client.name}" required readonly>
            <div class="valid-feedback">
                �Se ve bien!
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <label for="validationCustomLastNameRegister">Apellidos</label>
            <input type="text" class="form-control" name="LastNameRegister" id="validationCustomLastNameRegister" placeholder="Apellidos" value="${client.lastname}" required readonly>
            <div class="valid-feedback">
                �Se ve bien!
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <label for="validationCustomEmailRegister">Usuario</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend">@</span>
                </div>
                <input type="text" class="form-control" name="EmailRegister" id="validationCustomEmailRegister" placeholder="Usuario" value="${client.email}" aria-describedby="inputGroupPrepend" required readonly>
                <div class="invalid-feedback">
                    Por favor, elija un correo electr�nico.
                </div>
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <label for="validationCustomPasswordRegister">Contrase�a</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-key"></i></span>
                </div>
                <input type="password" class="form-control" name="PasswordRegister" id="validationCustomPasswordRegister" placeholder="Contrase�a" aria-describedby="inputGroupPrepend" required>
                <div class="invalid-feedback">
                    Por favor, elija una contrase�a.
                </div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-6 mb-3">
            <label for="validationCustomAddressRegister">Direcci�n</label>
            <input type="text" class="form-control" name="AddressRegister" id="validationCustomAddressRegister" placeholder="Direcci�n" value="${client.address}" required>
            <div class="invalid-feedback">
                Por favor, proporcione una direcci�n v�lida.
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <label for="validationCustomCityRegister">Ciudad</label>
            <input type="text" class="form-control" name="CityRegister" id="validationCustomCityRegister" placeholder="Ciudad" value="${client.city}" required>
            <div class="invalid-feedback">
                Por favor, proporciona una ciudad v�lida.
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <label for="validationCustomStateRegister">Provincia</label>
            <input type="text" class="form-control" name="StateRegister" id="validationCustomStateRegister" placeholder="Provincia" value="${client.province}" required>
            <div class="invalid-feedback">
                Por favor, proporcione un estado v�lido.
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <label for="validationCustomZipCodeRegister">C�digo Postal</label>
            <input type="text" class="form-control" name="ZipCodeRegister" id="validationCustomZipCodeRegister" placeholder="C�digo Postal" value="${client.zipCode}" required>
            <div class="invalid-feedback">
                Proporcione un c�digo postal v�lido.
            </div>
        </div>
        <div class="col-md-3">
            <label for="validationCustomPhoneRegister">Tel�fono</label>
            <input type="text" class="form-control" name="PhoneRegister" id="validationCustomPhoneRegister" placeholder="Tel�fono" value="${client.phone}" required>
            <div class="invalid-feedback">
                Proporcione un c�digo postal v�lido.
            </div>
        </div>
    </div>
    <button class="btn btn-success mt-3" type="submit">Guardar cambios</button>
    <button class="btn btn-primary mt-3" type="reset">Cancelar cambios</button>
</form>