<%@page import="sessionBeans.DetailFacade"%>
<%@page import="entities.Detail"%>
<%@page import="entities.Invoce"%>
<%@page import="purchase.PurchaseSessionBean"%>
<%@ include file="/includes/header.jsp" %>
<br><br>

<%
    PurchaseSessionBean psb = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/PurchaseSessionBean!purchase.PurchaseSessionBean");
    DetailFacade detailFacade = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/DetailFacade!sessionBeans.DetailFacade");
    List<Invoce> invoces = new ArrayList();

    // Tuve q hacer esta forma, xk cuando compro una nueva pelicula en el objeto invoce no se guarda DeatilList.
    for (Invoce elem : psb.getInvoces((Client) session.getAttribute("client"))) {
        elem.setDetailList(detailFacade.getDetailsFromInvoce(elem));
        invoces.add(elem);
    }
    pageContext.setAttribute("invoces", invoces);

    // Logs
    SingletonStatistics singletonStatistics = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/SingletonStatistics!singletons.SingletonStatistics");
    singletonStatistics.addVisitsPerPagesAndComponents("unknown.jsp");
    Client user = (Client) session.getAttribute("client");
    String userName = session.getId();

    if (user != null) {
        userName = user.getName() + " " + user.getLastname();
    }
    singletonStatistics.addComponetsPerUser(userName);
%>

<div class="container w3-theme-l5 w3-section">
    <div class="row">
        <div class="col-12 col-md-2 mt-0 mb-4 pl-0">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Pedidos</a>
                <a class="nav-link" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Mis datos</a>
                <a class="nav-link" id="v-pills-settings-tab" href="FrontController?command=LogoutCommand" role="tab">Cerrar sesi�n</a>
            </div>
        </div>
        <div class="col-12 col-md-10 mt-0 mb-4">
            <div class="tab-content" id="v-pills-tabContent">

                <!-- Pedidos -->
                <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                    <%@include file="includes/orders.jsp" %>
                </div>
                <!-- Fin pedidos -->

                <!-- Update user data Form -->
                <div class="tab-pane fade pt-4" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    <%@include file="includes/updateUsersForm.jsp" %>
                </div>
                <!-- End user data Form-->
            </div>
        </div>
    </div>
</div>

<%@ include file = "/includes/footer.jsp" %>