<%@ include file = "/includes/header.jsp" %>
<br><br>

<%
    SingletonStatistics singletonStatistics = InitialContext.doLookup("java:global/MoviesHD/MoviesHD-ejb/SingletonStatistics!singletons.SingletonStatistics");
    singletonStatistics.addVisitsPerPagesAndComponents("fantasy.jsp");
    Client user = (Client) session.getAttribute("client");
    String userName = session.getId();

    if (user != null) {
        userName = user.getName() + " " + user.getLastname();
    }
    singletonStatistics.addComponetsPerUser(userName);
%>

<div class="container mt-4">
    <%@ include file = "/includes/movies.jsp" %>
</div>

<%@ include file = "/includes/footer.jsp" %>




