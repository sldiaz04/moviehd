/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package websocket;

import model.Comment;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.json.spi.JsonProvider;
import javax.websocket.Session;

/**
 *
 * @author sldia
 */
@ApplicationScoped
public class CommentSessionHandler {
    private int commentId = 0;
    private final Set<Session> sessions = new HashSet<>();
    private final Set<Comment> comments = new HashSet<>();
    
    public void addSession(Session session) {
        sessions.add(session);
        comments.stream().map((comment) -> createAddMessage(comment)).forEachOrdered((addMessage) -> {
            sendToSession(session, addMessage);
        });
    }

    public void removeSession(Session session) {
        sessions.remove(session);
    }
    
    //  Methods that operate on the Comment object.
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    public void addComment(Comment comment) {
        comment.setId(commentId);
        comments.add(comment);
        commentId++;
        JsonObject addMessage = createAddMessage(comment);
        sendToAllConnectedSessions(addMessage);
    }

    private JsonObject createAddMessage(Comment comment) {
        JsonProvider provider = JsonProvider.provider();
        JsonObject addMessage = provider.createObjectBuilder()
                .add("action", "add")
                .add("id", comment.getId())
                .add("userName", comment.getUserName())
                .add("message", comment.getMessage())
                .build();
        return addMessage;
    }

    private void sendToAllConnectedSessions(JsonObject message) {
        sessions.forEach((session) -> {
            sendToSession(session, message);
        });
    }

    private void sendToSession(Session session, JsonObject message) {
        try {
            session.getBasicRemote().sendText(message.toString());
        } catch (IOException ex) {
            sessions.remove(session);
            Logger.getLogger(CommentSessionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
