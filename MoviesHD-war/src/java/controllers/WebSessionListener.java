/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import cart.CartLine;
import cart.CartSessionBean;
import entities.Cart;
import entities.Client;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import sessionBeans.CartFacade;

/**
 *
 * @author sldia
 */
public class WebSessionListener implements HttpSessionListener {

    CartFacade cartFacade = lookupCartFacadeBean();

    @Override
    public void sessionCreated(HttpSessionEvent hse) {
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
        ServletContext context = hse.getSession().getServletContext();
        synchronized (context) {
            HttpSession session = hse.getSession();
            CartSessionBean cartSessionBean = (CartSessionBean) session.getAttribute("cart");
            Client client = (Client) session.getAttribute("client");

            cartFacade.deleteCartsFromClient(client);
            
            if (cartSessionBean != null && !cartSessionBean.getItems().isEmpty()) {
                List<CartLine> cartLines = cartSessionBean.getItems();
                Cart cart = new Cart();

                for (CartLine cartLine : cartLines) {
                    cart.setIdClient(client);
                    cart.setIdMovie(cartLine.getMovie());
                    cart.setAmount(cartLine.getAmount());
                    cart.setDiscount(cartLine.getDiscount());
                    cart.setPrice(cartLine.getTotalPrice());
                    cartFacade.create(cart);
                }
            } 
        }
    }

    private CartFacade lookupCartFacadeBean() {
        try {
            Context c = new InitialContext();
            return (CartFacade) c.lookup("java:global/MoviesHD/MoviesHD-ejb/CartFacade!sessionBeans.CartFacade");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
