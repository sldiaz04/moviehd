/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import chat.model.Comment;
import entities.Client;
import java.io.IOException;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sessionBeans.ClientFacade;

/**
 *
 * @author sldia
 */
@WebServlet(name = "MessageDrivenServlet", urlPatterns = {"/MessageDrivenServlet"})
public class MessageDrivenServlet extends HttpServlet {

    @EJB
    private ClientFacade clientFacade;
    private static final Logger LOGGER = LogManager.getLogger("singleton");

    @Resource(mappedName = "jmsDemo/sergioDest")
    private Queue queue;

    @Resource(mappedName = "jmsDemo/sergio")
    private ConnectionFactory connectionFactory;

    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Client user;
        String userId = request.getParameter("userId");
        if (userId.equals("Anonymous")) {
            user = new Client();
            user.setName("Anonymous");
            user.setImageProfile("user-anonymous.png");
        }else{
            user = clientFacade.find(Integer.parseInt(userId));
            user.setImageProfile("user-laptop.png");
        }
        sendJMSMessageToSergioDest(request.getParameter("comment"), user);
        response.sendRedirect("chat.jsp");
    }
    
    private void sendJMSMessageToSergioDest(String messageData, Client user) {
        try {
            Connection connection = connectionFactory.createConnection();
            Session session = connection.createSession();
            MessageProducer messageProducer = session.createProducer(queue);
            ObjectMessage objectMessage = session.createObjectMessage(new Comment(user, messageData));
            messageProducer.send(objectMessage);
        } catch (JMSException ex) {
            LOGGER.error(messageData);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
