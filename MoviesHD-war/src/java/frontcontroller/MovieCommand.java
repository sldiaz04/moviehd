/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import entities.Client;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sessionBeans.MovieFacade;

/**
 *
 * @author sldia
 */
public class MovieCommand extends FrontCommand {

    MovieFacade movieFacade = lookupMovieFacadeBean();
    private static final Logger LOGGER = LogManager.getLogger("singleton");

    @Override
    public void process() {
        Client user = (Client) session.getAttribute("client");
        String userName = session.getId();

        if (user != null) {
            userName = user.getName() + " " + user.getLastname();
        }
        
        String genre = request.getParameter("genre");
        request.setAttribute("movies", movieFacade.findMoviesByGenre(genre, userName));
        forward("/GenreMoviePages/" + genre + ".jsp");
    }

    private MovieFacade lookupMovieFacadeBean() {
        try {
            Context c = new InitialContext();
            return (MovieFacade) c.lookup("java:global/MoviesHD/MoviesHD-ejb/MovieFacade!sessionBeans.MovieFacade");
        } catch (NamingException ne) {
            LOGGER.error("Exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
