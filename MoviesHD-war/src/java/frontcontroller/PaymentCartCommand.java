/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import cart.CartSessionBean;
import entities.Client;
import java.io.IOException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import purchase.PurchaseSessionBean;

/**
 *
 * @author sldia
 */
public class PaymentCartCommand  extends FrontCommand{

    PurchaseSessionBean purchase = lookupPurchaseBean();
    private static final Logger LOGGER = LogManager.getLogger("singleton");

    @Override
    public void process() {
        Client client = (Client) session.getAttribute("client");
        if (client == null) {// usuario no logeado
            forward("/loginAndRegister.jsp");
        }else{
            purchase.makePayment((CartSessionBean) session.getAttribute("cart"), (Client) session.getAttribute("client"));
            session.removeAttribute("cart");
            try {
                response.sendRedirect("useraccount.jsp");
            } catch (IOException ex) {
                LOGGER.error("Exception caught", ex);
            }
        }
    }

    private PurchaseSessionBean lookupPurchaseBean() {
        try {
            Context c = new InitialContext();
            return (PurchaseSessionBean) c.lookup("java:global/MoviesHD/MoviesHD-ejb/PurchaseSessionBean!purchase.PurchaseSessionBean");
        } catch (NamingException ne) {
            LOGGER.error("Exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
    
}
