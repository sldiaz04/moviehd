/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import entities.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sldia
 */
public class UnknownCommand extends FrontCommand{

    private static final Logger LOGGER = LogManager.getLogger("singleton.stateless");

    @Override
    public void process() {
        Client user = (Client) session.getAttribute("client");
        String userName = session.getId();

        if (user != null) {
            userName = user.getName() + " " + user.getLastname();
        }
        LOGGER.info(userName);
        forward("/unknown.jsp");
    }
    
}
