/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import java.io.IOException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import sessionBeans.MovieFacade;
import cart.CartLine;
import cart.CartSessionBean;
import entities.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sldia
 */
public class RemoveMovieCommand extends FrontCommand {

    MovieFacade movieFacade = lookupMovieFacadeBean();
    private static final Logger LOGGER = LogManager.getLogger("singleton");

    @Override
    public void process() {
        
        Client user = (Client) session.getAttribute("client");
        String userName = session.getId();

        if (user != null) {
            userName = user.getName() + " " + user.getLastname();
        }
        
        CartSessionBean cartSessionBean = (CartSessionBean) session.getAttribute("cart");
        if (request.getParameter("fulldelete") != null) {
            cartSessionBean.deleteMovie(new CartLine(movieFacade.find(Integer.parseInt(request.getParameter("id")))), userName);
        } else {
            cartSessionBean.remoMovie(new CartLine(movieFacade.find(Integer.parseInt(request.getParameter("id")))), userName);
        }
        session.setAttribute("cart", cartSessionBean);
        try {
            response.sendRedirect("cartDetails.jsp");
        } catch (IOException ex) {
            LOGGER.error("Exception caught", ex);
        }
    }

    private MovieFacade lookupMovieFacadeBean() {
        try {
            Context c = new InitialContext();
            return (MovieFacade) c.lookup("java:global/MoviesHD/MoviesHD-ejb/MovieFacade!sessionBeans.MovieFacade");
        } catch (NamingException ne) {
            LOGGER.error("Exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
