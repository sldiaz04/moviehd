/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import cart.CartLine;
import cart.CartSessionBean;
import entities.Cart;
import entities.Client;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import login.LoginSessionBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sessionBeans.CartFacade;
import stratergy.MovieYearStrategy;

/**
 *
 * @author sldia
 */
public class LoginCommand extends FrontCommand {

    CartFacade cartFacade = lookupCartFacadeBean();

    LoginSessionBean loginSessionBean = lookupLoginRegisterSessionBeanBean();
    private static final Logger LOGGER = LogManager.getLogger("singleton");

    
    @Override
    public void process() {
        Client client = (Client) session.getAttribute("client");
        if (client == null) {// usuario no logeado
            client = loginSessionBean.login(request.getParameter("loginEmail"), request.getParameter("loginPassword"), session.getId());
            if (client != null) {// Si existe el usuario y sus datos son correctos
                session.setAttribute("client", client);
                List<Cart>  carts = cartFacade.getCartsFromClient(client);
                if (!carts.isEmpty()) {
                    List<CartLine> cartLines = new ArrayList();
                    CartLine cartLine;
                    for (Cart cart : carts) {
                        cartLine = new CartLine(cart.getIdMovie(), new MovieYearStrategy());
                        cartLine.setAmount(cart.getAmount());
                        cartLine.setDiscount(cart.getDiscount());
                        cartLine.applyDiscount();
                        cartLines.add(cartLine);
                    }
                    CartSessionBean cartSessionBean = lookupCartSessionBeanBean();
                    cartSessionBean.addMovies(cartLines);
                    session.setAttribute("cart", cartSessionBean);
                }
                
                forward("/useraccount.jsp");
            } else {
                try {
                    response.sendRedirect("loginAndRegister.jsp");
                } catch (IOException ex) {
                    LOGGER.error("Exception caught", ex);
                }
            }
        } else {
            forward("/useraccount.jsp");
        }
    }

    private LoginSessionBean lookupLoginRegisterSessionBeanBean() {
        try {
            Context c = new InitialContext();
            return (LoginSessionBean) c.lookup("java:global/MoviesHD/MoviesHD-ejb/LoginSessionBean!login.LoginSessionBean");
        } catch (NamingException ne) {
            LOGGER.error("Exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private CartFacade lookupCartFacadeBean() {
        try {
            Context c = new InitialContext();
            return (CartFacade) c.lookup("java:global/MoviesHD/MoviesHD-ejb/CartFacade!sessionBeans.CartFacade");
        } catch (NamingException ne) {
            java.util.logging.Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private CartSessionBean lookupCartSessionBeanBean() {
        try {
            Context c = new InitialContext();
            return (CartSessionBean) c.lookup("java:global/MoviesHD/MoviesHD-ejb/CartSessionBean!cart.CartSessionBean");
        } catch (NamingException ne) {
            java.util.logging.Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
