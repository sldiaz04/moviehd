/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import entities.Client;
import java.io.IOException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import login.RegisterSessionBean;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sldia
 */
public class RegisterUserCommand extends FrontCommand {

    RegisterSessionBean registerSessionBean = lookupRegisterSessionBeanBean();
    private static final Logger LOGGER = LogManager.getLogger("singleton");

    @Override
    public void process() {
        Client client = (Client) session.getAttribute("client");
        if (client != null) {
            setClientData(client);
            if (registerSessionBean.updateClientData(client)) {
                session.setAttribute("client", client);
            }
            
            try {
                response.sendRedirect("useraccount.jsp");
            } catch (IOException ex) {
                LOGGER.error("Exception caught", ex);
            }
        } else {
            client = new Client();
            setClientData(client);
            if (registerSessionBean.register(client)) {
                session.setAttribute("client", client);
                forward("/useraccount.jsp");
            } else {
                forward("/loginAndRegister.jsp");
            }
        }
    }

    private void setClientData(Client client) {
        client.setName(request.getParameter("UserNameRegister"));
        client.setLastname(request.getParameter("LastNameRegister"));
        client.setEmail(request.getParameter("EmailRegister"));
        client.setPassword(DigestUtils.sha1Hex(request.getParameter("PasswordRegister")));
        client.setAddress(request.getParameter("AddressRegister"));
        client.setCity(request.getParameter("CityRegister"));
        client.setProvince(request.getParameter("StateRegister"));
        client.setZipCode(Integer.parseInt(request.getParameter("ZipCodeRegister")));
        client.setPhone(Integer.parseInt(request.getParameter("PhoneRegister")));
    }

    private RegisterSessionBean lookupRegisterSessionBeanBean() {
        try {
            Context c = new InitialContext();
            return (RegisterSessionBean) c.lookup("java:global/MoviesHD/MoviesHD-ejb/RegisterSessionBean!login.RegisterSessionBean");
        } catch (NamingException ne) {
            LOGGER.error("Exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
