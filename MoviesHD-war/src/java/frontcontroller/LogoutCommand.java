/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import entities.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sldia
 */
public class LogoutCommand extends FrontCommand{

    private static final Logger LOGGER = LogManager.getLogger("singleton.stateless");;
    
    @Override
    public void process() {
        Client user = (Client) session.getAttribute("client");
        LOGGER.info(user.getName() + " " + user.getLastname());
        session.invalidate();
        forward("/loginAndRegister.jsp");
    }
    
}
