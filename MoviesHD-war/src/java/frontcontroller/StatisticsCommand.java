/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sessionBeans.ApplicationLogFacade;

/**
 *
 * @author sldia
 */
public class StatisticsCommand extends FrontCommand {

    ApplicationLogFacade applicationLogFacade = lookupApplicationLogFacadeBean();
    private static final Logger LOGGER = LogManager.getLogger("singleton");

    @Override
    public void process() {
        String previous = request.getParameter("previous");
        String next = request.getParameter("next");
        int currentPage;
        try {
            currentPage = Integer.parseInt(request.getParameter("currentpage"));
        } catch (NumberFormatException e) {
            currentPage = 1;
        }

        int totalRegister = applicationLogFacade.count();
        double numOfPage = Math.ceil((double) totalRegister / 50);
        
        if (previous != null && Integer.parseInt(previous) > 1) {
            currentPage = Integer.parseInt(previous) - 1;
        } else if (next != null && Integer.parseInt(next) < numOfPage) {
            currentPage = Integer.parseInt(next) + 1;
        }
        
        int begin = (currentPage - 1) * 50;
        int end = begin + 49;
        int[] range = {begin, end};

        request.setAttribute("statistics", applicationLogFacade.findRange(range));
        request.setAttribute("totalregister", numOfPage);
        request.setAttribute("currentpage", currentPage);
        forward("/statistics.jsp");
    }

    private ApplicationLogFacade lookupApplicationLogFacadeBean() {
        try {
            Context c = new InitialContext();
            return (ApplicationLogFacade) c.lookup("java:global/MoviesHD/MoviesHD-ejb/ApplicationLogFacade!sessionBeans.ApplicationLogFacade");
        } catch (NamingException ne) {
            LOGGER.error("Exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
