/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import cart.CartSessionBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sldia
 */
public class CartDetailCommand extends FrontCommand {

    private static final Logger LOGGER = LogManager.getLogger("singleton");
    @Override
    public void process() {
        CartSessionBean cartSessionBean;
        if (session.getAttribute("cart") == null) {
            cartSessionBean = lookupCartSessionBeanBean();
            session.setAttribute("cart", cartSessionBean);
        }
        forward("/cartDetails.jsp");
    }

    private CartSessionBean lookupCartSessionBeanBean() {
        try {
            Context c = new InitialContext();
            return (CartSessionBean) c.lookup("java:global/MoviesHD/MoviesHD-ejb/CartSessionBean!cart.CartSessionBean");
        } catch (NamingException ne) {
            LOGGER.error("Exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
