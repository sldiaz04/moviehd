/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import sessionBeans.MovieFacade;

/**
 *
 * @author sldia
 */
public class ProductDetailCommand extends FrontCommand {

    MovieFacade movieFacade = lookupMovieFacadeBean();
    private static final Logger LOGGER = LogManager.getLogger("singleton");

    @Override
    public void process() {
        request.setAttribute("movie", movieFacade.find(Integer.parseInt(request.getParameter("image"))));
        forward("/productDetails.jsp");
    }

    private MovieFacade lookupMovieFacadeBean() {
        try {
            Context c = new InitialContext();
            return (MovieFacade) c.lookup("java:global/MoviesHD/MoviesHD-ejb/MovieFacade!sessionBeans.MovieFacade");
        } catch (NamingException ne) {
            LOGGER.error("Exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
