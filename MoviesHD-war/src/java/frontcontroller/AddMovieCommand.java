/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frontcontroller;

import java.io.IOException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import sessionBeans.MovieFacade;
import cart.CartLine;
import cart.CartSessionBean;
import entities.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import stratergy.MovieYearStrategy;

/**
 *
 * @author sldia
 */
public class AddMovieCommand extends FrontCommand {

    MovieFacade movieFacade = lookupMovieFacadeBean();
    private static final Logger LOGGER = LogManager.getLogger("singleton");

    @Override
    public void process() {
        Client user = (Client) session.getAttribute("client");
        String userName = session.getId();
        
        if (user != null) {
            userName = user.getName() + " " + user.getLastname();
        }
        
        CartSessionBean cartSessionBean;
        if (session.getAttribute("cart") == null) {
            cartSessionBean = lookupCartSessionBeanBean();
            session.setAttribute("cart", cartSessionBean);
        } else {
            cartSessionBean = (CartSessionBean) session.getAttribute("cart");
        }
        CartLine cartLine = new CartLine(movieFacade.find(Integer.parseInt(request.getParameter("id"))), new MovieYearStrategy());
        cartLine.applyDiscount();
        cartSessionBean.addMovie(cartLine, userName);
        try {
            if (request.getParameter("additem") != null) {
                response.sendRedirect("cartDetails.jsp");
            } else {
                response.sendRedirect("index.jsp");
            }
        } catch (IOException ex) {
            LOGGER.error(userName, ex);
        }
    }

    private CartSessionBean lookupCartSessionBeanBean() {
        try {
            Context c = new InitialContext();
            return (CartSessionBean) c.lookup("java:global/MoviesHD/MoviesHD-ejb/CartSessionBean!cart.CartSessionBean");
        } catch (NamingException ne) {
            LOGGER.error("Exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private MovieFacade lookupMovieFacadeBean() {
        try {
            Context c = new InitialContext();
            return (MovieFacade) c.lookup("java:global/MoviesHD/MoviesHD-ejb/MovieFacade!sessionBeans.MovieFacade");
        } catch (NamingException ne) {
            LOGGER.error("Exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
